package com.lonffrey.gupao.homework.designpattern.factory.abstr;

public class VolkswagenCarFactory implements ICarFactory {
    public ISedanCar createSedanCar() {
        return new VolkswagenSedanCar();
    }

    public ITricycleCar createTricycleCar() {
        return new VolkswagenTricycleCar();
    }
}
