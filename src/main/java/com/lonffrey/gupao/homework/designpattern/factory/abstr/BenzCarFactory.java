package com.lonffrey.gupao.homework.designpattern.factory.abstr;

public class BenzCarFactory implements ICarFactory {
    public ISedanCar createSedanCar() {
        return new BenzSedanCar();
    }

    public ITricycleCar createTricycleCar() {
        return new BenzTricycleCar();
    }
}
