package com.lonffrey.gupao.homework.designpattern.factory.method;

import com.lonffrey.gupao.homework.designpattern.factory.ICar;
import com.lonffrey.gupao.homework.designpattern.factory.TricycleCar;

/**
 * 三轮车工厂
 * @author Lonffrey
 * @date 2019/3/19 13:22
 */
public class TricycleCarFactory implements ICarFactory {
    public ICar createCar() {
        return new TricycleCar();
    }
}
