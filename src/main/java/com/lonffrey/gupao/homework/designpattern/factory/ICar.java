package com.lonffrey.gupao.homework.designpattern.factory;

public interface ICar {

    void printName();
}
