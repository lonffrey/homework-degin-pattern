package com.lonffrey.gupao.homework.designpattern.factory.method;

import com.lonffrey.gupao.homework.designpattern.factory.ICar;
import com.lonffrey.gupao.homework.designpattern.factory.SedanCar;

/**
 * 小轿车工厂
 * @author Lonffrey
 * @date 2019/3/19 13:21
 */
public class SedanCarFactory implements ICarFactory {

    public ICar createCar() {
        return new SedanCar();
    }
}
