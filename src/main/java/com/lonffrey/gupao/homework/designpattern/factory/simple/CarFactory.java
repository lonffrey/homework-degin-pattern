package com.lonffrey.gupao.homework.designpattern.factory.simple;

import com.lonffrey.gupao.homework.designpattern.factory.CarType;
import com.lonffrey.gupao.homework.designpattern.factory.ICar;
import com.lonffrey.gupao.homework.designpattern.factory.SedanCar;
import com.lonffrey.gupao.homework.designpattern.factory.TricycleCar;

public class CarFactory {

    public ICar createCar(CarType carType){
        if(CarType.SEDAN == carType){
            return new SedanCar();
        } else if (CarType.TRICYCLE == carType){
            return new TricycleCar();
        } else {
            throw new IllegalArgumentException("not support this car type");
        }
    }
}
