package com.lonffrey.gupao.homework.designpattern.factory.abstr;

public interface ICarFactory {

    ISedanCar createSedanCar();

    ITricycleCar createTricycleCar();
}
