package com.lonffrey.gupao.homework.designpattern.factory.method;

import com.lonffrey.gupao.homework.designpattern.factory.ICar;

/**
 * @author Lonffrey
 * @date 2019/3/19 13:20
 */
public interface ICarFactory {

    /**
     * 创建车
     * @return 车
     */
    ICar createCar();
}
