package com.lonffrey.gupao.homework.designpattern.factory;

public enum CarType {

    /**
     * 小轿车
     */
    SEDAN,

    /**
     * 三轮车
     */
    TRICYCLE;
}
